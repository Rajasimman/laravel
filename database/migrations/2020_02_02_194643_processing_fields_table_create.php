<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ProcessingFieldsTableCreate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('processing_field', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedInteger('field_id');
            $table->unsignedInteger('tractor_id');
			$table->unsignedInteger('user_id');
			$table->date('process_date');
			$table->double('area', 8, 2);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('processing_field');
    }
}
