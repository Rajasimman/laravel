<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\User;
use Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\DB;

class AgricultureController extends Controller
{
    public function create_fields(Request $request)
    {
		$validator = Validator::make($request->all(), [
         'name' 		=> 'required|string',
         'crops' 	=> 'required|string',
		 'area'		=> 'required|int',
  		]);
		
		if ($validator->fails()) {
			return response()->json(['errors' => $validator->errors()]);
		}
		
		DB::table('fields')->insert(
			['name' => $request->name, 'crops' => $request->crops, 'area' => $request->area, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s'), 'user_id' => Auth::user()->id]
		);
		
		return response()->json([
            'message' => 'Successfully created fields!'
        ], 201);
    }
	
	public function get_fields(Request $request)
    {
		$fields = DB::table('fields')->select('*')->get();
		return response()->json([
            'fields' => $fields
        ], 201);
	}
	
	public function create_tractors(Request $request)
    {
		$validator = Validator::make($request->all(), [
         'name' 	=> 'required|string',
  		]);
		
		if ($validator->fails()) {
			return response()->json(['errors' => $validator->errors()]);
		}
		
		DB::table('tractors')->insert(
			['name' => $request->name, 'user_id' => Auth::user()->id, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
		);
		
		return response()->json([
            'message' => 'Successfully created tractors!'
        ], 201);
    }
	
	public function get_tractors(Request $request)
    {
		$tractors = DB::table('tractors')->select('*')->get();
		return response()->json([
            'tractors' => $tractors
        ], 201);
	}
	
	public function processing_field(Request $request, $field_id, $tractor_id)
    {
		$validator = Validator::make($request->all(), [
         'process_date' 	=> 'required|date',
		 'area'		=> 'required|int',
  		]);
		
		if ($validator->fails()) {
			return response()->json(['errors' => $validator->errors()]);
		}
		
		$area = DB::select('select area from fields WHERE id= '.$field_id);
		
		if($request->area > $area['0']->area){
			return response()->json(['errors' => 'Area should not exceed the value : '.$area['0']->area]);
		}
		
		DB::table('processing_field')->insert(
			['field_id' => $field_id, 'tractor_id' => $tractor_id, 'user_id' => Auth::user()->id, 'process_date' => date("Y-m-d", strtotime($request->process_date)), 'area' => $request->area, 'created_at' => date('Y-m-d H:i:s'), 'updated_at' => date('Y-m-d H:i:s')]
		);
		
		return response()->json([
            'message' => 'Successfully created processing field!'
        ], 201);
    }
	
	public function get_processed_field_report(Request $request)
    {
		$validator = Validator::make($request->all(), [
         'process_date' 	=> 'date',
  		]);
		
		if ($validator->fails()) {
			return response()->json(['errors' => $validator->errors()]);
		}
		
		$total_processed_area = DB::table('processing_field')->sum('area');
		
		$processed_field_report = DB::select('SELECT f.name as field_name, crops as culture, t.name as tractor_name, pf.area as processed_area FROM `processing_field` as pf left join fields as f on pf.field_id = f.id left join tractors as t on pf.tractor_id = t.id WHERE is_approved = 1 '.(isset($request->field) && $request->field != '' ? ' and f.name LIKE "%'.$request->field.'%"' : '').(isset($request->culture) && $request->culture != '' ? ' and f.crops LIKE "%'.$request->culture.'%"' : '').(isset($request->process_date) && $request->process_date != '' ? ' and pf.process_date LIKE "'.date("Y-m-d", strtotime($request->process_date)).'"' : ''));
		
		if(isset($request->field) && $request->field != ''){
			$field_name = $request->field;
			$field_name_cond = '=';
		}else{
			$field_name = '';
			$field_name_cond = '!=';
		}
		
		$data = DB::table("processing_field")->select(DB::raw("SUM(processing_field.area) as total_processed_field"))->leftjoin("fields","processing_field.field_id","=","fields.id")->leftjoin("tractors","processing_field.tractor_id","=","tractors.id")->where('processing_field.is_approved', '=', 1)->groupBy("processing_field.id")->get();
		
		return response()->json([
            'processed_field_report' => $processed_field_report,
			'total_processed_area' => $data['0']->total_processed_field,
        ], 201);
	}
	
	public function process_field_approval(Request $request, $process_id)
    {
		if(Auth::user()->user_type != 2){
			return response()->json(['errors' => 'Only supervisor is allowed to access this page']);
		}
		
		$validator = Validator::make($request->all(), [
		 'is_approved'		=> 'required|boolean',
  		]);
		
		if ($validator->fails()) {
			return response()->json(['errors' => $validator->errors()]);
		}
		
		DB::table('processing_field')->where('id', $process_id)->update(['is_approved' => $request->is_approved]);
		//$area = DB::select('select area from fields WHERE id= '.$process_id);
		
		return response()->json([
            'message' => 'Successfully updated process field!'
        ], 201);
    }
	
	public function edit_fields(Request $request, $field_id)
    {
		if(Auth::user()->user_type != 3){
			$user_id = DB::select('SELECT user_id FROM fields WHERE id = '.$field_id);
			if($user_id['0']->user_id != Auth::user()->id){
				return response()->json(['errors' => 'You are not able to modify this field data']);
			}
		}
		
		$validator = Validator::make($request->all(), [
         'name' 		=> 'nullable|string',
         'crops' 	=> 'nullable|string',
		 'area'		=> 'nullable|int',
  		]);
		
		if ($validator->fails()) {
			return response()->json(['errors' => $validator->errors()]);
		}
		
		if($request->name == '' && $request->crops == '' && $request->area == ''){
			return response()->json(['errors' => 'All values are empty. Unable to update']);
		}
		
		//DB::table('processing_field')->where('id', $process_id)->update(['is_approved' => $request->is_approved]);
		$update_string = '';
		if(isset($request->name) && $request->name != ''){
			$update_string .= 'name = "'.$request->name.'"';
		}
		if(isset($request->crops) && $request->crops != ''){
			$update_string .= ($update_string != '' ? ',' : '').'crops = "'.$request->crops.'"';
		}
		if(isset($request->area) && $request->area != ''){
			$update_string .= ($update_string != '' ? ',' : '').'area = '.$request->area;
		}
		
		DB::statement('UPDATE fields SET '.$update_string.',updated_at = "'.date('Y-m-d H:i:s').'" where id = '.$field_id);
		
		return response()->json([
            'message' => 'Successfully updated fields!'
        ], 201);
    }
}
