<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::post('signup', 'Api\AuthController@signup');

Route::group([
    'prefix' => 'auth'
], function () {
    Route::post('login', 'Api\AuthController@login');
    Route::post('user_signup', 'Api\AuthController@user_signup');
	Route::post('supervisor_signup', 'Api\AuthController@supervisor_signup');
	Route::post('admin_signup', 'Api\AuthController@admin_signup');
  
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
        Route::get('logout', 'Api\AuthController@logout');
        Route::get('user', 'Api\AuthController@user');
    });
});

Route::group([
    'prefix' => 'fields'
], function () {
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
		Route::post('create_fields', 'Api\AgricultureController@create_fields');
		Route::get('get_fields', 'Api\AgricultureController@get_fields');
    });
});

Route::group([
    'prefix' => 'tractors'
], function () {
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
		Route::post('create_tractors', 'Api\AgricultureController@create_tractors');
		Route::get('get_tractors', 'Api\AgricultureController@get_tractors');
    });
});

Route::group([
    'prefix' => 'process'
], function () {
    Route::group([
      'middleware' => 'auth:api'
    ], function() {
		Route::post('processing_field/{field_id}/{tractor_id}', 'Api\AgricultureController@processing_field');
		Route::get('get_processed_field_report', 'Api\AgricultureController@get_processed_field_report');
		Route::post('process_field_approval/{process_id}', 'Api\AgricultureController@process_field_approval');
		Route::post('edit_fields/{field_id}', 'Api\AgricultureController@edit_fields');
    });
});
